package db

import (
	"geochat/auth/models"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
)

var Instance *gorm.DB

func Connect(dsn string) {
	var err error
	Instance, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connected to Database")
}

func Migrate() {
	err := Instance.AutoMigrate(&models.User{})
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Database Migration Completed")
}
