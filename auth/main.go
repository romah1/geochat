package main

import (
	"fmt"
	"geochat/auth/db"
	"geochat/auth/handlers"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"log"
	"os"
)

func init() {
	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

func main() {
	dbUser, exists := os.LookupEnv("DATABASE_USERNAME")
	if !exists {
		log.Fatal("no database user in env")
	}
	dbPassword, exists := os.LookupEnv("DATABASE_PASSWORD")
	if !exists {
		log.Fatal("no database password in env")
	}
	dbHost, exists := os.LookupEnv("DATABASE_HOST")
	if !exists {
		log.Fatal("no database host in env")
	}
	dbPort, exists := os.LookupEnv("DATABASE_PORT")
	if !exists {
		log.Fatal("no database port in env")
	}
	dbName, exists := os.LookupEnv("DATABASE_NAME")
	if !exists {
		log.Fatal("no database name in env")
	}

	db.Connect(fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbUser, dbPassword, dbHost, dbPort, dbName))
	db.Migrate()

	engine := setupGinEngine()
	err := engine.Run(":8080")
	if err != nil {
		panic(err)
	}
}

func setupGinEngine() *gin.Engine {
	engine := gin.Default()
	api := engine.Group("/api")

	// users
	api.POST("/users/register", handlers.RegisterUser)
	api.POST("/users/login", handlers.LoginUser)

	// health
	api.GET("/health", handlers.Health)

	return engine
}
